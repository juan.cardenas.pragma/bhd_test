export const environment = {
  production: true,
  endpoints: {
    auth: 'https://bhdleonfrontend-test.herokuapp.com/sign_in',
    userData: 'https://bhdleonfrontend-test.herokuapp.com/user_data',
    products: {
      accounts: 'https://bhdleonfrontend-test.herokuapp.com/products/accounts',
      credit_cards: 'https://bhdleonfrontend-test.herokuapp.com/products/credit_cards'
    }
  }
};
