import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { NotificationsService } from '../notifications/notifications.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ErrorHandlerInterceptor implements ErrorHandler {

  constructor(
    private injector: Injector,
  ) { }

  /**
   * Controla los errores por lado del servidor
   * 
   * Next Feature: Agregar el de Cliente y App
   *
   * @param {(Error | HttpErrorResponse)} error
   * @returns
   * @memberof ErrorHandlerInterceptor
   */
  handleError(error: Error | HttpErrorResponse) {
    const notificationService = this.injector.get(NotificationsService);

    if (error instanceof HttpErrorResponse) {
      if (!navigator.onLine) {
        // No Internet connection
        return notificationService.notify('No Internet Connection');
      }
      // Mostrar notificacion al usuario
      return notificationService.notify(`${error.status} - ${error.message}`);
    }
  }
}
