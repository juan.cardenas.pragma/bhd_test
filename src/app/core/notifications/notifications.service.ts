import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { publish, refCount } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class NotificationsService {

  private _notification: BehaviorSubject<any> = new BehaviorSubject(null);
  readonly notification$: Observable<string> = this._notification.asObservable()
    .pipe(
      publish(),
      refCount()
    )

  constructor() { }
  /**
   * Evento para mandar el catchError al componente de notificaciones
   *
   * @param {*} message
   * @memberof NotificationsService
   */
  public notify(message: any) {
    this._notification.next(message);
    setTimeout(() => this._notification.next(null), 3000);
  }
}
