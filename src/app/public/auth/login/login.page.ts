import { Component, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { NavController, LoadingController, MenuController } from '@ionic/angular';
import { NotificationsService } from 'src/app/core/notifications/notifications.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  private unsubscribe$ = new Subject<void>();

  public loginForm: FormGroup;
  public routes = [
    {
      name: 'Contactos',
      icon: 'assets/images/icon_contact.svg',
      link: '/contact'
    },
    {
      name: 'Sucursales',
      icon: 'assets/images/icon_branches.svg',
      link: '/branches'
    }
  ];

  constructor(
    private fb: FormBuilder,
    private __authService: AuthService,
    public navCtrl: NavController,
    private loadCtrl: LoadingController,
    public menuCtrl: MenuController,
    private injector: Injector
  ) {
    const userId = atob(localStorage.getItem('userInfo') || '');
    this.loginForm = this.fb.group({
      userId: [userId, Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      remember: [Boolean(userId)]
    })
  }

  /**
   * Metodo para login user
   *
   * @param {{ userId: string, password: string, remember: boolean }} values
   * @memberof LoginPage
   */
  public async loginUser(values: { userId: string, password: string, remember: boolean }) {
    const { userId, password } = values;
    const credentials = { userId, password };
    const loading = await this.loadCtrl.create();
    await loading.present();
    this.__authService.login(credentials)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(async () => {
        if (values.remember) {
          localStorage.setItem('userInfo', btoa(values.userId));
        } else {
          localStorage.removeItem('userInfo');
        }
        await loading.dismiss();
        await this.navCtrl.navigateRoot('/dashboard');
      }, async (err) => {
        const notificationService = this.injector.get(NotificationsService);
        notificationService.notify(err.error.message);
        await loading.dismiss()
      }, async () => await loading.dismiss());
  }

  /**
   * Unsubscribe Observables
   *
   * @memberof LoginPage
   */
  ionViewWillLeave(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
