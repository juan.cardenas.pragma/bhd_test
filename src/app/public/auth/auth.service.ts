import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from "rxjs/operators";
import { SignInResponse } from './auth.type';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Guarda el token en un sessionStorage
   *
   * @private
   * @param {string} token
   * @memberof AuthService
   */
  private setStorageToken(token: string) {
    sessionStorage.setItem('token', token);
  };

  /**
   * Retorna el token del sessionStorage
   *
   * @returns
   * @memberof AuthService
   */
  public getTokenStorage() {
    return sessionStorage.getItem('token');
  }

  /**
   * Service: autentificacion
   *
   * @param {{ userId: string, password: string }} values
   * @returns
   * @memberof AuthService
   */
  public login(values: { userId: string, password: string }) {
    return this.http.post<SignInResponse>(environment.endpoints.auth, values)
      .pipe(
        map(res => {
          this.setStorageToken(res.access_token);
          return res.access_token;
        })
      );
  }
}
