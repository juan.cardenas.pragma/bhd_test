import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { NavController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class LoginGuard implements CanActivate {

  /**
   * Guard: Validacion del token en el sessionStorage
   * 
   * @param {AuthService} __authService
   * @param {NavController} navCtrl
   * @memberof LoginGuard
   */
  constructor(
    private __authService: AuthService,
    private navCtrl: NavController
  ) { }

  canActivate(): boolean {
    const tkn = this.__authService.getTokenStorage();
    if (tkn) {
      return true;
    } else {
      this.navCtrl.navigateRoot('/login');
    }
  }
}
