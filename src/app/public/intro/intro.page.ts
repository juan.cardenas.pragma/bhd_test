import { Component, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage {

  @ViewChild('introSlider', { static: true }) private slides: IonSlides;

  public slideOpt = {};
  public slidesArg = [{
    title: 'Puedes ver tus productos',
    image: 'assets/images/slide_1.svg'
  }, {
    title: 'Ahorra tiempo realizando transacciones de forma rápida y segura',
    image: 'assets/images/slide_2.svg'
  }, {
    title: 'Aprovecha las ofertas que tenemos para ti',
    image: 'assets/images/slide_3.svg'
  }]

  constructor(
    private navCtrl: NavController
  ) { }

  /**
   * Next Slide
   *
   * @memberof IntroPage
   */
  public async swipeNext() {
    const countSlides = this.slidesArg.length - 1;
    const index = await this.slides.getActiveIndex();

    if (countSlides === index) {
      await this.navCtrl.navigateForward('/login');
      return;
    }

    await this.slides.slideNext();
  }

}
