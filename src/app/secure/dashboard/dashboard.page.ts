import { Component } from '@angular/core';
import { NavController, MenuController, LoadingController } from '@ionic/angular';
import { DashboardService } from './dashboard.service';
import { UserDataResponse } from './dashboard.types';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage {

  private unsubscribe$ = new Subject<void>();

  public dataUser: UserDataResponse;

  public routes = [
    {
      name: 'Mis Productos',
      link: 'dashboard/products',
      linkTab: 'products',
      icon: 'assets/images/icon_my_products_green.svg',
      iconTab: 'assets/images/icon_account_gray.svg'
    },
    {
      name: 'Transacciones',
      link: 'dashboard/transactions',
      linkTab: 'transactions',
      icon: 'assets/images/icon_transactions_green.svg',
      iconTab: 'assets/images/icon_transactions_gray.svg'
    },
    {
      name: 'Ofertas',
      link: 'dashboard/offers',
      linkTab: 'offers',
      icon: 'assets/images/icon_offers_green.svg',
      iconTab: 'assets/images/icon_offers_gray.svg'
    },
    {
      name: 'Configuración',
      link: 'dashboard/settings',
      linkTab: 'settings',
      icon: 'assets/images/icon_config_green.svg',
      iconTab: 'assets/images/icon_config_gray.svg'
    }
  ];

  public routesSecondary = [
    {
      name: 'Contactos',
      icon: 'assets/images/icon_contact.svg',
      link: '/contact'
    },
    {
      name: 'Sucursales',
      icon: 'assets/images/icon_branches.svg',
      link: '/branches'
    }
  ];

  constructor(
    public navCtrl: NavController,
    private menuCtrl: MenuController,
    private __dashboardService: DashboardService,
    private loadCtrl: LoadingController
  ) { }

  async ionViewDidEnter() {
    const load = await this.loadCtrl.create();
    await load.present();
    this.__dashboardService.getUserData()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(async (data) => {
        this.dataUser = data;
        await load.dismiss();
      })
  }

  async goToPage(route) {
    await this.navCtrl.navigateRoot(route.link);
    await this.menuCtrl.close();
  }

  public logout() {
    sessionStorage.clear();
    this.navCtrl.navigateRoot('/login');
  }

  ionViewWillLeave() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
