export interface UserDataResponse {
    name:     string;
    lastName: string;
    photo:    string;
}
