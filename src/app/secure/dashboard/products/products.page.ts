import { Component } from '@angular/core';
import { ProductsService } from './products.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { AccountResponse, CreditCardResponse } from './products.type';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DetailProductPage } from './detail-product/detail-product.page';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage {

  private unsubscribe$ = new Subject<void>();
  public accounts: AccountResponse[] = [];
  public creditCards: CreditCardResponse[] = [];

  constructor(
    private __producsService: ProductsService,
    private loadCtrl: LoadingController,
    private modalCtrl: ModalController
  ) { }

  /**
   * Inicializa los servicios de ProductsPage
   *
   * @memberof ProductsPage
   */
  async ionViewDidEnter() {
    const loading = await this.loadCtrl.create();
    await loading.present();
    this.__producsService.getProducts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(async (data) => {
        this.accounts = data.accounts;
        this.creditCards = data.credits;
        await loading.dismiss();
      });
  }

  /**
   * Despliegue de modal del DetailProduct
   *
   * @param {AccountResponse} info
   * @returns
   * @memberof ProductsPage
   */
  async showDetail(info: AccountResponse) {
    const modal = await this.modalCtrl.create({
      component: DetailProductPage,
      componentProps: {
        info
      }
    });

    return await modal.present();
  }

  /**
   * Finalizacion del ciclo de vida del componente y
   * Unsubscribe de Observables
   *
   * @memberof ProductsPage
   */
  public ionViewWillLeave(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
