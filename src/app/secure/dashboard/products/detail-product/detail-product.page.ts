import { Component } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { AccountResponse } from '../products.type';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.page.html',
  styleUrls: ['./detail-product.page.scss'],
})
export class DetailProductPage {

  public account: AccountResponse;
  /**
   * Modal -> DetailProduct of ProductsPage
   * 
   * @param {ModalController} modalCtrl
   * @param {NavParams} navParams
   * @memberof DetailProductPage
   */
  constructor(
    public modalCtrl: ModalController,
    private navParams: NavParams
  ) {
    this.account = this.navParams.data.info;
  }

}
