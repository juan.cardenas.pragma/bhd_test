import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AccountResponse, CreditCardResponse, ProductsResponse } from './products.type';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProductsService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Service: Traer cuentas
   *
   * @private
   * @returns {Observable<AccountResponse[]>}
   * @memberof ProductsService
   */
  private getAccounts(): Observable<AccountResponse[]> {
    return this.http.get<AccountResponse[]>(environment.endpoints.products.accounts);
  }
  /**
   * Service: Traer tarjetas de credito
   *
   * @private
   * @returns
   * @memberof ProductsService
   */
  private getCreditCards() {
    return this.http.get<CreditCardResponse[]>(environment.endpoints.products.credit_cards);
  }

  /**
   * Service: Retorna las cuentas y tarjetas de credito
   *
   * @returns {Observable<ProductsResponse>}
   * @memberof ProductsService
   */
  public getProducts(): Observable<ProductsResponse> {
    return forkJoin<AccountResponse[], CreditCardResponse[]>([this.getAccounts(), this.getCreditCards()])
      .pipe(
        map(res => {
          return {
            accounts: res[0],
            credits: res[1]
          }
        })
      )
  }

}
