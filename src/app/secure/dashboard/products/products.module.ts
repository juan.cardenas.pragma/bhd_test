import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsPageRoutingModule } from './products-routing.module';

import { ProductsPage } from './products.page';
import { DetailProductPage } from './detail-product/detail-product.page';
import { DetailProductPageModule } from './detail-product/detail-product.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsPageRoutingModule,
    DetailProductPageModule
  ],
  declarations: [
    ProductsPage
  ]
})
export class ProductsPageModule {}
