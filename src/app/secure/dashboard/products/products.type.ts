export interface AccountResponse {
    alias: string;
    number: string;
    availableAmount: number;
    productType: string;
}

export interface CreditCardResponse {
    alias: string;
    number: string;
    availableAmountRD: number;
    availableAmountUS: number;
    isInternational: boolean;
    productType: string;
}

export interface ProductsResponse {
    accounts: AccountResponse[],
    credits: CreditCardResponse[]
}