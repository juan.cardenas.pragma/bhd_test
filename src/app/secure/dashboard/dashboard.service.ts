import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserDataResponse } from './dashboard.types';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private http: HttpClient
  ) { }

  public getUserData(): Observable<UserDataResponse> {
    return this.http.get<UserDataResponse>(environment.endpoints.userData);
  }
}
